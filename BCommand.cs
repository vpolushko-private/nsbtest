﻿using NServiceBus;

namespace NsbTest
{
	public class BCommand : ICommand
	{
		public int Number { get; set; }
	}
}

﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NServiceBus;
using NServiceBus.Extensions.Logging;
using NServiceBus.Logging;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace NsbTest
{
	class Program
	{
		static void Main(string[] args)
		{

			Host.CreateDefaultBuilder()
				.ConfigureAppConfiguration(builder =>
				{
					string configFile = "appsettings.json";
					builder
						.AddJsonFile(configFile, optional: false, reloadOnChange: true);

				})
				.ConfigureLogging(logBuilder =>
				{
					logBuilder.AddNLog("nlog.config");

				}).UseConsoleLifetime()
				.ConfigureServices((hostContext, services) =>
				{

					services.Configure<MyConfig>(hostContext.Configuration.GetSection("MyConfig"));

				})
				.UseNServiceBus(hostBuilderContext =>
				{

					var endpoint = new EndpointConfiguration("test.queue");

					var transport = endpoint.UseTransport<RabbitMQTransport>();

					var configSection = hostBuilderContext.Configuration.GetSection("MyConfig");
					var config = configSection.Get<MyConfig>();

					var connectionString = config.RabbitConnectionString;
					transport.ConnectionString(connectionString);

					var routing = transport.Routing();
					routing.RouteToEndpoint(messageType: typeof(ACommand), destination: "test.queue");
					routing.RouteToEndpoint(messageType: typeof(BCommand), destination: "test.queue");

					endpoint.EnableInstallers();
					transport.UseConventionalRoutingTopology();


					Microsoft.Extensions.Logging.ILoggerFactory extensionsLoggerFactory = new NLogLoggerFactory();
					NServiceBus.Logging.ILoggerFactory nservicebusLoggerFactory = new ExtensionsLoggerFactory(extensionsLoggerFactory);
					LogManager.UseFactory(nservicebusLoggerFactory);

					return endpoint;
				})
				.Build().Run();
		}
	}
}

﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NServiceBus;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace NsbTest
{
	public class TestHandler : IHandleMessages<ACommand>, IHandleMessages<BCommand>
	{
		private IOptionsMonitor<MyConfig> _config;
		private ILogger<TestHandler> _logger;
		public TestHandler(IOptionsMonitor<MyConfig> config, ILogger<TestHandler> logger)
		{
		
			_logger = logger;
			_config = config;
		}

		public async Task Handle(ACommand message, IMessageHandlerContext context)
		{

			_logger.Log(LogLevel.Information, $"Start sending {_config.CurrentValue.SendMessagesCount} messages");
			for (int i = 0; i < _config.CurrentValue.SendMessagesCount; i++)
			{
				await context.Send<BCommand>(x =>
				{
					x.Number = i;
				});
			}

			_logger.Log(LogLevel.Information, $"FinishSendingMessages");
		}

		public Task Handle(BCommand message, IMessageHandlerContext context)
		{
			Thread.Sleep(500);
			return Task.CompletedTask;
		}
	}
}

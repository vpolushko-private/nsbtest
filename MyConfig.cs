﻿namespace NsbTest
{
	public class MyConfig
	{
		public string RabbitConnectionString { get; set; }
		public int SendMessagesCount { get; set; }
	}
}
